package com.cio.currency;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.junit.Assert;


public class CurrencyPair
{

	private String base = null;

	private String delt = null;

	private static final String SEPERATOR = "/";

	private static List<String> validPairs = new ArrayList<>(Arrays.asList("EUR", "USD", "INR", "JPY", "AUD"));
	
	
	public static float inrusd = (float) 0.012;
	private static float inreur = (float) 0.011;
	private static float inrjpy = (float) 1.93;
	private static float inraud = (float) 0.018;

	public CurrencyPair(String curr1, String curr2)
	{
		Objects.requireNonNull(curr1,"Currency pair is null");
		Objects.requireNonNull(curr2,"Currency pair 2 is null");
		this.base = curr1;
		this.delt = curr2;

	}
	
	
	public CurrencyPair()
	{
		
	}
	

	public CurrencyPair(String currPair)
	{

		Objects.requireNonNull(currPair,"Currency pair is null");
		Assert.assertTrue(currPair.length() <= 7);
		String[] split = currPair.split(SEPERATOR);
		this.base = split[0];
		this.delt = split[1];
	}
//
	public boolean isValidCurrency(String curr)
	{

		Objects.nonNull(curr);
		return validPairs.contains(curr.toUpperCase());
	}

	public String getCurrencyPair()
	{
		return base + SEPERATOR + delt;
	}

	public String getBase()
	{
		return base;
	}

	public void setBase(String base)
	{
		this.base = base;
	}

	public String getDelt()
	{
		return delt;
	}

	public void setDelt(String delt)
	{
		this.delt = delt;
	}
	
	
	   public static double inrconvertor(int amount, String base) {
	        double convertedValue = 0;

	        switch (base) {
	            case "USD":
	                convertedValue = amount * inrusd;
	                break;
	            case "EUR":
	                convertedValue = amount * inreur;
	                break;
	            case "JPY":
	                convertedValue = amount * inrjpy;
	                break;
	            case "AUD":
	                convertedValue = amount * inraud;
	                break;
	            default:
	                System.out.println("Invalid currency code");
	                break;
	        }

	        return convertedValue;
	    }
	
	
	
    public static double handleUserInput(String input) {
        // Split the input string
        String[] parts = input.split(" ");
        
        
        int amount = Integer.parseInt(parts[0]);
        
        if (parts[0].contains("-")) {
        	amount = 0;
        }

        
        
        String pair = parts[1].toUpperCase();

        // Split the currency pair
        String[] currencies = pair.split("/");
        String fromCurrency = currencies[0];
        String toCurrency = currencies[1];

        // Validate the input
        double result=0;
        try {
			if (validPairs.contains(fromCurrency) && validPairs.contains(toCurrency) && "INR".equals(fromCurrency))  {
			    result = inrconvertor(amount, toCurrency);
			    System.out.println(result);
//			    System.out.println(amount + " " + fromCurrency + " is equal to " + result + toCurrency );
			}else {
			    System.out.println("Invalid input or unsupported currency conversion");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
    }
	
	
	

	public static void main(String[] args)
	{
		
		CurrencyPair prashantTest = new CurrencyPair("INR/USD");
		
		
//        String input = "-900 INR/USD";
//        handleUserInput(input);
		
		
		
	}
}
