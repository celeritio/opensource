package com.cio.currency;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CurrencyPairTest
{

	
	
	@Test
	public void testOnly7Char() 
	{
	CurrencyPair aPair = new CurrencyPair("JPY" ,"INR");
		
	String baseCurr = aPair.getBase();
	
	String deltCurr = aPair.getDelt();
	Assert.assertTrue(aPair.isValidCurrency(baseCurr)); 
	Assert.assertTrue(aPair.isValidCurrency(deltCurr)); 
		
	}
	
	
	@Test
	public void testInrToUsd() {
		CurrencyPair aPair = new CurrencyPair();
		
		String inrvalue1 = "85000 INR/USD";
		String inrvalue2 = "0 INR/USD";
		String inrvalue3 = "-1 INR/USD";
		
		double expectedvalue = 1020.0;
		double expectedvalue2 = 0.0;
		double expectedvalue3 = 0.0;
		
		double result1 = CurrencyPair.handleUserInput(inrvalue1);
		double result2 = CurrencyPair.handleUserInput(inrvalue2);
		double result3 = CurrencyPair.handleUserInput(inrvalue3);
		
		Assertions.assertEquals(expectedvalue, result1);
		Assertions.assertEquals(expectedvalue2, result2);
		Assertions.assertEquals(expectedvalue3, result3);


	}
	
}
