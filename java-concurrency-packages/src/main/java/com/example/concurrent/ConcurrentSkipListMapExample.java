package com.example.concurrent;

import java.util.Iterator;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;

import org.apache.log4j.Logger;

public class ConcurrentSkipListMapExample {
	/*
	 * We can access the elements of a ConcurrentSkipListMap using the get() method,
	 */
	private static final Logger log = Logger.getLogger(ConcurrentSkipListMapExample.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ConcurrentMap<Integer, String> chm = new ConcurrentSkipListMap<Integer, String>();

		// insert mappings using put method
		chm.put(100, "first");
		chm.put(101, "second");
		chm.put(102, "Third");
		chm.put(103, "Fourth");

		// Displaying the HashMap
		log.info("The Mappings are: ");
		log.info(chm);

		// Display the value of 100
		log.info("The Value associated to " + "100 is : " + chm.get(100));

		// Getting the value of 103
		log.info("The Value associated to " + "103 is : " + chm.get(103));
		// Create an Iterator over the
		// ConcurrentSkipListMap
		Iterator<ConcurrentSkipListMap.Entry<Integer, String>> itr = chm.entrySet().iterator();
		// The hasNext() method is used to check if there is
		// a next element The next() method is used to
		// retrieve the next element
		while (itr.hasNext()) {
			ConcurrentSkipListMap.Entry<Integer, String> entry = itr.next();
			log.info("Key = " + entry.getKey() + ", Value = " + entry.getValue());
		}
	}

}
