package com.example.concurrent;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.log4j.Logger;

public class ConcurrentHashMapExample {
	// adding logger
	private static final Logger log = Logger.getLogger(ConcurrentHashMapExample.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Since ConcurrentMap is an interface,
		// we create instance using ConcurrentHashMap
		ConcurrentMap<Integer, String> m = new ConcurrentHashMap<Integer, String>();
		m.put(100, "Shubhangi");
		m.put(101, "payal");
		m.put(102, "anjali");

		// Here we cant add Hello because 101 key
		// is already present
		m.putIfAbsent(101, "Hello");

		// We can remove entry because 101 key
		// is associated with For value
		m.remove(101, "payal");

		// Now we can add Hello
		m.putIfAbsent(101, "riya");

		// We can replace Hello with For
		m.replace(101, "riya", "shreya");
		log.info("Map contents : " + m);
	}

}
