package com.example.concurrent;

import java.util.HashMap;

import org.apache.log4j.Logger;

public class HashMapExample extends Thread {
	private static final Logger log = Logger.getLogger(HashMapExample.class);

	static HashMap<Integer, String> l = new HashMap<Integer, String>();

	public void run() {
		try {
			// Thread.sleep(1000);
			// Child thread trying to add
			// new element in the object
			l.put(103, "D");
			for (Object o : l.entrySet()) {
				Object s = o;
				log.info(s);
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		l.put(100, "A");
		l.put(101, "B");
		l.put(102, "C");

		HashMapExample t = new HashMapExample();
		t.start();

		log.info(l);

	}

}
