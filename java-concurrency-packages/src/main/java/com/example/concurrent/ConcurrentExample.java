package com.example.concurrent;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

public class ConcurrentExample extends Thread {
	private static final Logger log = Logger.getLogger(ConcurrentExample.class);

	static ConcurrentHashMap<Integer, String> l = new ConcurrentHashMap<Integer, String>();

	public void run() {

// Child add new element in the object
		l.put(103, "D");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			log.info("Child Thread going to add element");
		}
	}

	public static void main(String[] args) throws InterruptedException {
		l.put(100, "A");
		l.put(101, "B");
		l.put(102, "C");
		ConcurrentExample t = new ConcurrentExample();
		t.start();

		for (Object o : l.entrySet()) {
			Object s = o;
			log.info(s);
			Thread.sleep(1000);
		}
		log.info(l);
	}

}
