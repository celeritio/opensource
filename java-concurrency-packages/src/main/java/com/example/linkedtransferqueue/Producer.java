package com.example.linkedtransferqueue;

import java.util.Random;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class Producer implements Runnable {
	private static final Logger log = Logger.getLogger(Producer.class);

	LinkedTransferQueue<Integer> linkedTransferQueue = new LinkedTransferQueue<Integer>();
	private Random random = new Random();

	public Producer(LinkedTransferQueue<Integer> linkedTransferQueue, int i) {
		super();
		this.linkedTransferQueue = linkedTransferQueue;
		this.random = random;
	}

	public void run() {
		// TODO Auto-generated metho private static final Logger log =
		// Logger.getLogger(Consumer.class); private static final Logger log =
		// Logger.getLogger(Consumer.class);d stub
		try {
			while (true) {
				log.info("Producer is waiting to transfer message...");

				Integer message = random.nextInt();
				boolean added = linkedTransferQueue.tryTransfer(message);
				if (added) {
					log.info("Producer added the message - " + message);
				}
				Thread.sleep(TimeUnit.SECONDS.toMillis(3));
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
