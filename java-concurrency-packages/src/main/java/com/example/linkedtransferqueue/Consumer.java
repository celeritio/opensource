package com.example.linkedtransferqueue;

import java.util.Random;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class Consumer implements Runnable {
	// adding logger
	private static final Logger log = Logger.getLogger(Consumer.class);

	LinkedTransferQueue<Integer> linkedTransferQueue = new LinkedTransferQueue<Integer>();
	private Random random = new Random();

	public Consumer(LinkedTransferQueue<Integer> linkedTransferQueue, int i) {
		super();
		this.linkedTransferQueue = linkedTransferQueue;
		this.random = random;
	}

	public void run() {
		// TODO Auto-generated method stub
		try {
			while (true) {
				log.info("Consumer is waiting to take message...");

				Integer message = linkedTransferQueue.take();

				log.info("Consumer recieved the message - " + message);

				Thread.sleep(TimeUnit.SECONDS.toMillis(3));
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
