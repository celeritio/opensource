package com.example.linkedtransferqueue;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedTransferQueue;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		LinkedTransferQueue<Integer> linkedTransferQueue = new LinkedTransferQueue<Integer>();
		
		ExecutorService pes = Executors.newFixedThreadPool(2);
		ExecutorService ces = Executors.newFixedThreadPool(2);

		pes.execute(new Producer(linkedTransferQueue, 1));
		pes.execute(new Producer(linkedTransferQueue, 2));
		ces.execute(new Consumer(linkedTransferQueue, 1));
		ces.execute(new Consumer(linkedTransferQueue, 2));

		pes.shutdown();
		ces.shutdown();
	}

}
