package com.example.blockingq.producerConcumer;

import java.util.concurrent.BlockingQueue;
import org.apache.log4j.Logger;

public class Consumer implements Runnable {
	// adding logger
	private static final Logger log = Logger.getLogger(Consumer.class);

	private final BlockingQueue<Integer> sharedQueue;

	private int threadNo;

	public Consumer(BlockingQueue<Integer> sharedQueue, int threadNo) {
		this.sharedQueue = sharedQueue;
		this.threadNo = threadNo;
	}

	public void run() {
		// Consumer consumes numbers generated from Producer threads continuously
		while (true) {
			try {
				int num = sharedQueue.take();
				log.info("Consumed: " + num + ":by thread:" + threadNo);
			} catch (Exception err) {
				err.printStackTrace();
			}
		}
	}
}
