package com.example.blockingq.producerConcumer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/* this program for LinkedBlockingQueue,ArrayBlockingQueu and Priority BlockingQueue*/
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub

		BlockingQueue<Integer> sharedQueue = new LinkedBlockingQueue<Integer>();
		// BlockingQueue<Integer> sharedQueue = new ArrayBlockingQueue<Integer>(10);
		// BlockingQueue<Integer> sharedQueue = new PriorityBlockingQueue<Integer>();

		ExecutorService pes = Executors.newFixedThreadPool(2);
		ExecutorService ces = Executors.newFixedThreadPool(2);

		pes.execute(new Producer(sharedQueue, 1));
		pes.execute(new Producer(sharedQueue, 2));
		ces.execute(new Consumer(sharedQueue, 1));
		ces.execute(new Consumer(sharedQueue, 2));

		pes.shutdown();
		ces.shutdown();

	}

}
