package com.example.blockingq.producerConcumer;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import org.apache.log4j.Logger;

public class Producer implements Runnable {
	// adding logger
	private static final Logger log = Logger.getLogger(Producer.class);

	// sleep millis
	int millis = 200;

	private final BlockingQueue<Integer> sharedQueue;

	private int threadNo;

	private Random random = new Random();

	public Producer(BlockingQueue<Integer> sharedQueue, int threadNo) {
		this.threadNo = threadNo;
		this.sharedQueue = sharedQueue;
	}

	public void run() {
		// Producer produces a continuous stream of numbers for every 200 milli seconds
		while (true) {
			try {
				int number = random.nextInt(1000);
				log.info("Produced:" + number + ":by thread:" + threadNo);
				sharedQueue.put(number);
				Thread.sleep(millis);
			} catch (Exception err) {
				err.printStackTrace();
			}
		}
	}
}
