package com.example.executormethods;

import java.util.Queue;

import org.apache.log4j.Logger;

public class Consumer extends Thread {
	// adding logger
	private static final Logger log = Logger.getLogger(Consumer.class);
	private Queue<Integer> queue;
	private int maxSize;

	public Consumer(Queue<Integer> queue, int maxSize, String name) {
		super(name);
		this.queue = queue;
		this.maxSize = maxSize;
	}

	@Override
	public void run() {
		while (true) {
			synchronized (queue) {
				while (queue.isEmpty()) {
					log.info("Queue is empty," + "Consumer thread is waiting"
							+ " for producer thread to put something in queue");
					try {
						queue.wait();
					} catch (Exception ex) {
						ex.printStackTrace();
					}

				}
				log.info("Consuming value : " + queue.remove());
				queue.notifyAll();
			}

		}
	}
}
