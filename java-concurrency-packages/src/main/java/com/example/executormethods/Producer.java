package com.example.executormethods;

import java.util.Queue;
import java.util.Random;

import org.apache.log4j.Logger;

import com.example.blockingq.producerConcumer.Consumer;

public class Producer extends Thread {
	// adding logger
	private static final Logger log = Logger.getLogger(Consumer.class);
	private Queue<Integer> queue;
	private int maxSize;

	public Producer(Queue<Integer> queue, int maxSize, String name) {
		super(name);
		this.queue = queue;
		this.maxSize = maxSize;
	}

	public void run() {
		while (true) {
			synchronized (queue) {
				while (queue.size() == maxSize) {
					try {
						log.info("Queue is full, " + "Producer thread waiting for "
								+ "consumer to take something from queue");
						queue.wait();
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}

				Random random = new Random();
				int i = random.nextInt();
				log.info("Producing value : " + i);
				queue.add(i);
				queue.notifyAll();
			}

		}
	}

}
