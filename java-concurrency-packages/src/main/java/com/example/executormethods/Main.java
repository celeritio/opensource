package com.example.executormethods;

import java.util.LinkedList;
import java.util.Queue;

import org.apache.log4j.Logger;

public class Main {
	// adding logger
	private static final Logger log = Logger.getLogger(Main.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		log.info("How to use wait and notify method in Java");
		log.info("Solving Producer Consumper Problem");

		Queue<Integer> buffer = new LinkedList<Integer>();
		int maxSize = 10;

		Thread producer = new Producer(buffer, maxSize, "PRODUCER");
		Thread consumer = new Consumer(buffer, maxSize, "CONSUMER");

		producer.start();
		consumer.start();
	}
}
