package com.example.executor;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

public class ExecutorMehod2Example {
	private static final Logger log = Logger.getLogger(ExecutorMehod2Example.class);

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		// TODO Auto-generated method stub
		/*
		 * invokeAny() method executes the given list of tasks, returning the result of
		 * one that has completed successfully (i.e., without throwing an exception), if
		 * any do.
		 */
		ExecutorService executorService = Executors.newSingleThreadExecutor();

		Set<Callable<String>> callables = new HashSet<Callable<String>>();

		callables.add(new Callable<String>() {
			public String call() throws Exception {
				return "Task 2";
			}

		});
		callables.add(new Callable<String>() {
			public String call() throws Exception {
				return "Task 1";
			}
		});
		callables.add(new Callable<String>() {
			public String call() throws Exception {
				return "Task 3";
			}
		});

		String result = executorService.invokeAny(callables);

		log.info("result = " + result);

		executorService.shutdown();
	}

}
