package com.example.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

public class ExecutorInterfaceExample {
	private static final Logger log = Logger.getLogger(ExecutorInterfaceExample.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Creates a ExecutorService object having a pool of 10 threads.

		ExecutorService executorService = Executors.newFixedThreadPool(10);
		executorService.execute(new Runnable() {

			public void run() {
				log.info("ExecutorService for creating pool of 10 threads");

			}
		});

		// Creates a ExecutorService object having a single thread.

		ExecutorService executorService1 = Executors.newSingleThreadExecutor();
		executorService.execute(new Runnable() {

			public void run() {
				log.info("ExecutorService for craeting  single thread");
			}
		});

		// Creates a scheduled thread pool executor with 10 threads. In scheduled thread
		// //pool, we can schedule tasks of the threads.
		ExecutorService executorService2 = Executors.newScheduledThreadPool(10);
		executorService.execute(new Runnable() {

			public void run() {
				log.info("ExecutorService for craeting  schedule thread");
			}
		});
		/*
		 * Once we are done with our tasks given to ExecutorService, then we have to
		 * shut it down because ExecutorService performs the task on different threads.
		 * If we don't shut down the ExecutorService, the threads will keep running, and
		 * the JVM won?t shut down. The process of shutting down can be done by the
		 * following three methods-
		 * 
		 * shutdown() method shutdownNow() method awaitTermination() method
		 */
		executorService.shutdown();
		executorService1.shutdown();
		executorService2.shutdown();

	}

}
