package com.example.executor;

import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class ScheduledThreadPoolExecutorExample {
	private static final Logger log = Logger.getLogger(ScheduledThreadPoolExecutorExample.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		log.info("A count-down-clock program that counts from 10 to 0");

		// creating a ScheduledExecutorService object
		ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(11);

		// printing the current time
		log.info("Current time : " + Calendar.getInstance().get(Calendar.SECOND));

		// Scheduling the tasks
		for (int i = 10; i >= 0; i--) {
			scheduler.schedule(new Task(i), 10 - i, TimeUnit.SECONDS);
		}

		// remember to shutdown the scheduler
		// so that it no longer accepts
		// any new tasks
		scheduler.shutdown();
	}
}

class Task implements Runnable {
	private static final Logger log = Logger.getLogger(ScheduledThreadPoolExecutorExample.class);
	private int num;

	public Task(int num) {
		this.num = num;
	}

	public void run() {
		log.info("Number " + num + " Current time : " + Calendar.getInstance().get(Calendar.SECOND));
	}

}
