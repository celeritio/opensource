package com.example.executor;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

public class ExecutorMethodExample {

	private static final Logger log = Logger.getLogger(ExecutorMethodExample.class);

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		// TODO Auto-generated method stub
		/*
		 * invokeAll() This method executes the list of tasks given and returns the list
		 * of Futures which contain the results of all the tasks when completed.
		 */
		ExecutorService executorService = Executors.newSingleThreadExecutor();

		Set<Callable<String>> callables = new HashSet<Callable<String>>();

		callables.add(new Callable<String>() {
			public String call() throws Exception {
				return "Task 1";
			}
		});
		callables.add(new Callable<String>() {
			public String call() throws Exception {
				return "Task 2";
			}
		});
		callables.add(new Callable<String>() {
			public String call() throws Exception {
				return "Task 3";
			}
		});

		java.util.List<Future<String>> futures = executorService.invokeAll(callables);

		for (Future<String> future : futures) {
			log.info("future.get = " + future.get());
		}
		executorService.shutdown();

	}

}
