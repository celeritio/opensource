package com.kntx.loader;

import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;
/**
 * Start Jconsole  and from that Connect to the Startup.java class
 * you can insert an Order via Jconsole the signature should match to method
 * {@link ct.common.spring.example5.TradeEntryMBean.bookOrder}
 * @author nikhil
 *
 */
public class LEILoaderStartup 
{
	
	private static final Logger log = Logger.getLogger(LEILoaderStartup.class);
	
	private static final CountDownLatch latch = new CountDownLatch(1);

	public static void main(String[] args) 
	{
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("app-context.xml");

		log.info("[ Context Initilized.]");
		
		StartBanner();
		
		context.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread() 
		{
			@Override
			public void run() 
			{

				context.close();
			
				log.info("[****************************************]");
				log.info("[\t * Applicaton closed *\t\t]");
				log.info("[****************************************]");
			}
		});

		context.registerShutdownHook();

		
		
		
		//await();
		
		
	}

	private static void StartBanner()
	{
		log.info("[****************************************]");
		log.info("[\t * Application Started *\t\t]");
		log.info("[****************************************]");
	}

	public static void shutDown()
    {
        latch.countDown();
    }

    /**
     * Await the reception of the shutdown signal.
     */
    public static void await()
    {
        try
        {
            latch.await();
        }
        catch (final InterruptedException ignore)
        {
            Thread.currentThread().interrupt();
        }
    }
	
}