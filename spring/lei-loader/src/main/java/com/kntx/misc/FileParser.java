package com.kntx.misc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.jdbc.core.JdbcTemplate;

import com.kntx.builder.SQLInsertGenerator;

public class FileParser
{

	private JdbcTemplate jdbcTemplate;

	private static final Logger log = Logger.getLogger(FileParser.class);

	/**
	 * 
	 * @param fileName      The file to parse and read only columns in
	 *                      <code>columnsToRead<code>
	 * @param columnsToRead : columsn to read in a csv file
	 * @param sqlStatement  : a sql statement with '?' equivalent to the values
	 *                      matching in Columns to read
	 * 
	 * 
	 */
	public void leiLoader(String fileName, List<String> columnsToRead, String sqlStatement) {
	    // Get the file from the classpath
	    ClassLoader classLoader = FileParser.class.getClassLoader();
	    
	    try (InputStream inputStream = classLoader.getResourceAsStream(fileName);
	         BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	         CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader())) {

	        // List to store SQL statements for batch execution
	        List<String> batchSqlStatements = new ArrayList<>();

	        // Iterate over each record
	        for (CSVRecord record : csvParser) {
	            List<String> valuesForEachRow = new ArrayList<>();
	            Map<String, String> recordMap = record.toMap();

	            try {
	                // Process each column
	                for (String column : columnsToRead) {
	                    if (recordMap.containsKey(column)) {
	                        String value = recordMap.get(column);
	                        // Escape chars not allowed in insert and update
	                        valuesForEachRow.add(escapeForSql(value));
	                    } else {
	                        log.warn("Column not found in CSV: " + column);
	                    }
	                }

	                // Generate the SQL statement for this row
	                String finalSql = SQLInsertGenerator.replaceFielDWithValues(sqlStatement, valuesForEachRow);
	                 batchSqlStatements.add(finalSql); // Add the SQL statement to the batch

	                // Execute batch update after every 100 rows (adjust batch size as needed)
	                if (batchSqlStatements.size() == 100) {
	                    executeBatch(batchSqlStatements);
	                    batchSqlStatements.clear(); // Clear the batch after execution
	                }

	            } catch (Exception ex) {
	                log.warn("Exception for a row, skipping this row: ", ex);
	            }
	        }

	        // Execute any remaining SQL statements in the batch
	        if (!batchSqlStatements.isEmpty()) {
	            executeBatch(batchSqlStatements);
	        }

	    } catch (IOException e) {
	        log.warn("Exception in parsing file: ", e);
	    }
	}

	/**
	 * Helper method to execute a batch of SQL statements using JdbcTemplate.
	 */
	private void executeBatch(List<String> sqlStatements) {
	    try {
	        jdbcTemplate.batchUpdate(sqlStatements.toArray(new String[0]));
	        log.info("Batch executed successfully");
	    } catch (Exception e) {
	        log.warn("Batch execution failed: ", e);
	    }
	}


	public static String escapeForSql(String input)
	{
		if (input == null || input.isEmpty())
		{
			return input;
		}

		// Replace single quotes with two single quotes (SQL escaping rule)
		String escapedString = input.replace("'", "''");

		// Add any other custom escape/padding rules here
		// For example: Replacing double quotes or non-ASCII characters with a safe
		// version
		// (you can define the rules as per your requirements)

		// Example: Replacing smart quotes or other special characters (optional)
		escapedString = escapedString.replace("\u201C", "\"") // Left double quotation mark
				.replace("\u201D", "\"") // Right double quotation mark
				.replace("\u2605", "*"); // Unicode star symbol (☆) replaced with '*'

		return escapedString;
	}



	@Required
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{
		this.jdbcTemplate = jdbcTemplate;
	}
}
