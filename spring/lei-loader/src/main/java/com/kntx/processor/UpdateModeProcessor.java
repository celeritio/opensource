package com.kntx.processor;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.kntx.builder.SQLBuilder;
import com.kntx.misc.FileParser;

public class UpdateModeProcessor
{

	private SQLBuilder sqlBuilder;

	private static List<String> leiMasterColumns;

	private static List<String> leiCSVColumns;

	private static List<String> leiRelationshipCSVColumns;

	private static List<String> leiMasterParentColumns;

	public static String keyColumn = "LEI";

	private FileParser csvParser;

	private static String tableName = "LEI_MASTER";

	private static final Logger log = Logger.getLogger(SQLBuilder.class);

	public void processUpdates(String leiDetailsfile, String leiParentDetailsFile)
	{

		String stmtWithQue = SQLBuilder.buildUpdateStatement(tableName, leiMasterColumns, keyColumn);
		log.info("Update statement Created = " + stmtWithQue);

		csvParser.leiLoader(leiDetailsfile, leiCSVColumns, stmtWithQue);

		String updateWithQues = SQLBuilder.buildUpdateStatement(tableName, leiMasterParentColumns,
				UpdateModeProcessor.keyColumn);
		log.info("Update statement created" + updateWithQues);

		csvParser.leiLoader(leiParentDetailsFile, leiRelationshipCSVColumns, updateWithQues);

	}

	public SQLBuilder getSqlBuilder()
	{
		return sqlBuilder;
	}

	@Required
	public void setSqlBuilder(SQLBuilder sqlBuilder)
	{
		this.sqlBuilder = sqlBuilder;
	}

	public List<String> getLeiMasterColumns()
	{
		return leiMasterColumns;
	}

	@Required
	public void setLeiMasterColumns(List<String> leiMasterColumns)
	{
		this.leiMasterColumns = leiMasterColumns;
	}

	public String getKeyColumn()
	{
		return keyColumn;
	}

	public void setKeyColumn(String keyColumn)
	{
		UpdateModeProcessor.keyColumn = keyColumn;
	}

	public static List<String> getLeiCSVColumns()
	{
		return leiCSVColumns;
	}

	public static void setLeiCSVColumns(List<String> leiCSVColumns)
	{
		UpdateModeProcessor.leiCSVColumns = leiCSVColumns;
	}

	public FileParser getCsvParser()
	{
		return csvParser;
	}

	public void setCsvParser(FileParser csvParser)
	{
		this.csvParser = csvParser;
	}

	public List<String> getLeiRelationshipCSVColumns()
	{
		return leiRelationshipCSVColumns;
	}

	public void setLeiRelationshipCSVColumns(List<String> leiRelationshipCSVColumns)
	{
		UpdateModeProcessor.leiRelationshipCSVColumns = leiRelationshipCSVColumns;
	}

	public List<String> getLeiMasterParentColumns()
	{
		return leiMasterParentColumns;
	}

	public void setLeiMasterParentColumns(List<String> leiMasterParentColumns)
	{
		UpdateModeProcessor.leiMasterParentColumns = leiMasterParentColumns;
	}
}
