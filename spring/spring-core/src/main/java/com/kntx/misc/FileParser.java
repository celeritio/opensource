package com.kntx.misc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;

import com.kntx.builder.SQLBuilder;
import com.kntx.builder.SQLInsertGenerator;

public class FileParser
{

	private JdbcTemplate jdbcTemplate;

	private static final Logger log = Logger.getLogger(SQLBuilder.class);

	/**
	 * 
	 * @param fileName      The file to parse and read only columns in
	 *                      <code>columnsToRead<code>
	 * @param columnsToRead : columsn to read in a csv file
	 * @param sqlStatement  : a sql statement with '?' equivalent to the values
	 *                      matching in Columns to read
	 * 
	 * 
	 */
	public void leiLoader(String fileName, List<String> columnsToRead, String sqlStatement)
	{

		// Get the file from the classpath
		ClassLoader classLoader = FileParser.class.getClassLoader();
		try (InputStream inputStream = classLoader.getResourceAsStream(fileName);
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
				CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader()))
		{

			// Iterate over each record
			for (CSVRecord record : csvParser)
			{

				List<String> valuesForEachRow = new ArrayList<String>();
				// Access the record as a Map
				Map<String, String> recordMap = record.toMap();

				try
				{
					// Process each column
					for (String column : columnsToRead)
					{

						if (recordMap.containsKey(column))
						{
							String value = recordMap.get(column);
							valuesForEachRow.add(value);
						} else
						{
							log.warn("[didn't find the column in csv =" + column);
						}
					}
					String finalSql = SQLInsertGenerator.replaceFielDWithValues(sqlStatement, valuesForEachRow);
					jdbcTemplate.update(finalSql, new PreparedStatementSetter() {
						public void setValues(PreparedStatement ps) throws SQLException
						{
							
						}
					});

					
					
					log.info("final Statement created " + finalSql);

				} catch (Exception ex)
				{

					log.warn("Catching Exception for a Row Failed to Process and eating Change code if you want to read.",ex);
				}
			}

		} catch (IOException e)
		{
			log.warn("Exception inparsing file", e);
		}

	}

	@Required
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{
		this.jdbcTemplate = jdbcTemplate;
	}
}
