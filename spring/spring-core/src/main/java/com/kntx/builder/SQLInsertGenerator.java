package com.kntx.builder;

import java.util.ArrayList;
import java.util.List;

public class SQLInsertGenerator
{

	public static void main(String[] args)
	{
		try
		{
			String sqlTemplate = "INSERT INTO users (name, age, email) VALUES (?, ?, ?)";
			List<String> values = new ArrayList<>();
			values.add("John Doe");
			values.add("30");
			values.add("john.doe@example.com");

			String finalSql = replaceFielDWithValues(sqlTemplate, values);
			System.out.println(finalSql);
		} catch (IllegalArgumentException e)
		{
			System.err.println("Error: " + e.getMessage());
		}
	}

	public static String replaceFielDWithValues(String sqlTemplate, List<String> values)
	{
		if (sqlTemplate == null || sqlTemplate.isEmpty())
		{
			throw new IllegalArgumentException("SQL template cannot be null or empty.");
		}

		if (values == null || values.isEmpty())
		{
			throw new IllegalArgumentException("Values list cannot be null or empty.");
		}

		// Count the number of placeholders in the SQL template
		int placeholderCount = countPlaceholders(sqlTemplate);

		if (placeholderCount != values.size())
		{
			throw new IllegalArgumentException(
					"The number of placeholders does not match the number of values provided. PlaceHolder count =" + placeholderCount + "Values count =" + values.size());
		}

		StringBuilder finalSql = new StringBuilder();
		int valueIndex = 0;

		for (int i = 0; i < sqlTemplate.length(); i++)
		{
			char currentChar = sqlTemplate.charAt(i);
			if (currentChar == '?')
			{
				finalSql.append("'").append(values.get(valueIndex++)).append("'");
			} else
			{
				finalSql.append(currentChar);
			}
		}

		return finalSql.toString();
	}

	private static int countPlaceholders(String sqlTemplate)
	{
		int count = 0;
		for (char c : sqlTemplate.toCharArray())
		{
			if (c == '?')
			{
				count++;
			}
		}
		return count;
	}
}
