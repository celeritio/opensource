package com.kntx.builder;

import java.util.Arrays;
import java.util.List;

public class SQLBuilder
{



	// Method to generate an UPDATE statement
	public static String buildUpdateStatement(String tableName, List<String> columns,String keyColumn)
	{
		StringBuilder sql = new StringBuilder();

		sql.append("UPDATE ").append(tableName).append(" SET ");
		for (int i = 0; i < columns.size(); i++)
		{
			sql.append(columns.get(i)).append(" = ?");
			if (i < columns.size() - 1)
			{
				sql.append(", ");
			}
		}
		sql.append(" WHERE ").append(keyColumn).append(" = ?;");

		return sql.toString();
	}

	/**
	 *  Creates a insert static statement with 
	 * @param tableName
	 * @param columns
	 * @return
	 */
	public static String buildInsertStatement(String tableName, List<String> columns)
	{
		StringBuilder sql = new StringBuilder();

		sql.append("INSERT INTO ").append(tableName).append(" (");
		for (int i = 0; i < columns.size(); i++)
		{
			sql.append(columns.get(i));
			if (i < columns.size() - 1)
			{
				sql.append(", ");
			}
		}
		sql.append(") VALUES (");
		for (int i = 0; i < columns.size(); i++)
		{
			sql.append("?");
			if (i < columns.size() - 1)
			{
				sql.append(", ");
			}
		}
		sql.append(");");
//		sql.append("where );");

		return sql.toString();
	}

	public static void main(String[] args)
	{
		List<String> columns = Arrays.asList("column1", "column2", "column3");
		String tableName = "LEI_MASTER";
		String keyColumn = "column1"; // The unique constraint or primary key column

		String updateSql = buildUpdateStatement(tableName, columns,keyColumn);
		String insertSql = buildInsertStatement(tableName, columns);

		System.out.println("Update SQL: " + updateSql);
		System.out.println("Insert SQL: " + insertSql);
	}

	
}
