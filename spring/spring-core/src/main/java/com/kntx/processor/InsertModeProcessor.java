package com.kntx.processor;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.jdbc.core.JdbcTemplate;

import com.kntx.builder.SQLBuilder;
import com.kntx.misc.FileParser;

public class InsertModeProcessor
{

	private SQLBuilder sqlBuilder;



	private static List<String> leiMasterColumns;

	private static List<String> leiCSVColumns;

	private static List<String> leiRelationshipCSVColumns;

	private static List<String> leiMasterParentColumns;

	public static String keyColumn = "LEI";

	private static String tableName = "LEI_MASTER";

	private static final Logger log = Logger.getLogger(SQLBuilder.class);

	private FileParser csvParser;

	public void processInserts(String leiDetailsfile, String leiParentDetailsFile)
	{

		String insStmtWithQue = SQLBuilder.buildInsertStatement(tableName, leiMasterColumns);
		log.info("Insert statement Created = " + insStmtWithQue);

		csvParser.leiLoader(leiDetailsfile, leiCSVColumns, insStmtWithQue);
		log.info("insert statement created" + insStmtWithQue);

		String updateWithQues = SQLBuilder.buildUpdateStatement(tableName, leiMasterParentColumns, InsertModeProcessor.keyColumn);
		
		csvParser.leiLoader(leiParentDetailsFile, leiRelationshipCSVColumns, updateWithQues);

	}

	public List<String> getLeiMasterColumns()
	{
		return leiMasterColumns;
	}

	public void setLeiMasterColumns(List<String> leiMasterColumns)
	{
		this.leiMasterColumns = leiMasterColumns;
	}

	public String getKeyColumn()
	{
		return keyColumn;
	}

	public void setKeyColumn(String keyColumn)
	{
		InsertModeProcessor.keyColumn = keyColumn;
	}

	public SQLBuilder getSqlBuilder()
	{
		return sqlBuilder;
	}

	@Required
	public void setSqlBuilder(SQLBuilder sqlBuilder)
	{
		this.sqlBuilder = sqlBuilder;
	}

	public String getTableName()
	{
		return tableName;
	}

	public void setTableName(String tableName)
	{
		InsertModeProcessor.tableName = tableName;
	}

	public List<String> getLeiCSVColumns()
	{
		return leiCSVColumns;
	}

	@Required
	public void setLeiCSVColumns(List<String> leiCSVColumns)
	{
		InsertModeProcessor.leiCSVColumns = leiCSVColumns;
	}

	public FileParser getCsvParser()
	{
		return csvParser;
	}

	@Required
	public void setCsvParser(FileParser csvParser)
	{
		this.csvParser = csvParser;
	}

	public List<String> getLeiRelationshipCSVColumns()
	{
		return leiRelationshipCSVColumns;
	}

	public void setLeiRelationshipCSVColumns(List<String> leiRelationshipCSVColumns)
	{
		InsertModeProcessor.leiRelationshipCSVColumns = leiRelationshipCSVColumns;
	}

	public List<String> getLeiMasterParentColumns()
	{
		return leiMasterParentColumns;
	}

	public void setLeiMasterParentColumns(List<String> leiMasterParentColumns)
	{
		InsertModeProcessor.leiMasterParentColumns = leiMasterParentColumns;
	}

}
