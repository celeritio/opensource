package com.kntx.loader;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.Lifecycle;

import com.kntx.processor.InsertModeProcessor;
import com.kntx.processor.UpdateModeProcessor;

import ct.common.spring.example5.Application;

public class LEILoader implements Lifecycle
{

	private static final Logger log = Logger.getLogger(Application.class);

	private boolean isRunning = false;

	private static String mode = "INSERT";

	private InsertModeProcessor insertModeProcessor;

	private UpdateModeProcessor updateModeProcessor;

	private static final String LEI_DETAILS = "goldencopy.csv";

	private static final String LEI_PARENT_DETAILS = "goldencopy-rr.csv";

	/**
	 * Can be used to initialize your Application startups if its Multi-threaded the
	 * thread Pools or managers can be Initialized here
	 */
	@Override
	public void start()
	{
		if ("INSERT".equalsIgnoreCase(mode))
		{

			insertModeProcessor.processInserts(LEI_DETAILS, LEI_PARENT_DETAILS);
		} else if ("update".equalsIgnoreCase(mode))
		{
			updateModeProcessor.processUpdates(LEI_DETAILS, LEI_DETAILS);
		} else
		{
			throw new IllegalArgumentException(" Incorrect Mode Given: " + mode);
		}
		isRunning = true;
	}

	/**
	 * Cleanup or shutdown hooks for application specific resources can be added
	 * here.
	 */
	@Override
	public void stop()
	{
		log.info("[Stop of bean implementing the LifeCycle Interface Called ]");
		isRunning = false;

	}

	@Override
	public boolean isRunning()
	{
		return isRunning;
	}

	public String getMode()
	{
		return mode;
	}

	@Required
	public void setMode(String mode)
	{
		this.mode = mode;
	}

	public InsertModeProcessor getInsertModeProcessor()
	{
		return insertModeProcessor;
	}

	public void setInsertModeProcessor(InsertModeProcessor insertModeProcessor)
	{
		this.insertModeProcessor = insertModeProcessor;
	}

	public UpdateModeProcessor getUpdateModeProcessor()
	{
		return updateModeProcessor;
	}

	public void setUpdateModeProcessor(UpdateModeProcessor updateModeProcessor)
	{
		this.updateModeProcessor = updateModeProcessor;
	}


}