package ct.common.spring.example4;

public interface TransactionService {
	/**
	 * @param senderAccountNumber	: Account Number of Sender
	 * @param receiverAccountNumber	: Account Number of Receiver
	 * @param amountToSend			: Amount  to be sent to Receiver
	 *  */
	void transferAmount(int senderAccountNumber, int receiverAccountNumber, int amountToSend);

	
}
