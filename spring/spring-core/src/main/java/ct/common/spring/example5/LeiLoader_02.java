package ct.common.spring.example5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.StringTokenizer;

public class LeiLoader_02
{
//	private static final Logger log = LoggerFactory.getLogger(LeiLoader.class);
	public static void readLEIs(File file) {
		
		try
		{	
//			log.info()
			BufferedReader br = new BufferedReader(new FileReader(file));
            String strLine = "";
            StringTokenizer st = null;
            int lineNumber = 0, tokenNumber = 0;

            while((strLine = br.readLine()) != null)
            {
                lineNumber++;
                st = new StringTokenizer(strLine, ",");

                while(st.hasMoreTokens())
                {
	                tokenNumber++;
	                System.out.println("Line # " + lineNumber +
                        ", Token # " + tokenNumber
                        + ", Token : "+ st.nextToken());
                }
            }
		}
		catch (Exception e)
		{
			e.printStackTrace();
		} 
	
	}

}


