package ct.common.spring.example5;

import java.io.File;

import org.apache.log4j.Logger;
import org.springframework.context.Lifecycle;

public class Application implements Lifecycle
{

	private static final Logger log = Logger.getLogger(Application.class);

	private boolean isRunning = false;

	//private JdbcTemplate jdbcTemplate = null;
	
	/**
	 * Can be used to initialize your Application startups
	 * if its Multi-threaded the thread Pools or managers can be Initialized here
	 */
	@Override
	public void start()
	{
		log.info("[Start of bean implementing the LifeCycle Interface Called ]");
		try {
			
		File file = new File("C:\\mydata\\git\\opensource\\docs-LEI-KNTX\\Few-goldencopy.csv");
		
//		File file = new File("C:\\mydata\\git\\opensource\\docs-LEI-KNTX\\3.6\\20240904-0800-gleif-goldencopy-lei2-golden-copy.csv");
			CSVColumnReader.leiLoader();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		isRunning = true;
	}

	/**
	 * Cleanup or shutdown hooks for application specific resources can be added
	 * here.
	 */
	@Override
	public void stop()
	{
		log.info("[Stop of bean implementing the LifeCycle Interface Called ]");
		isRunning = false;

	}

	@Override
	public boolean isRunning()
	{
		return isRunning;
	}

}
