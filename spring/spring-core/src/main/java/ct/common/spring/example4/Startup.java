package ct.common.spring.example4;

import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author kiran
 */
public class Startup {

	private int senderAccountNumber = 1001;
	private int receiverAccountNumber = 2001;
	private int amountToSend = 1;

	private static final Logger log = Logger.getLogger(Startup.class);
	
	private static final CountDownLatch latch = new CountDownLatch(1);

	public static void main(String[] args) {
		Startup executionObj = new Startup();

		try {
			log.info("[ context initializing.. ]");

			ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.example4.xml");
			context.start();
			log.info("[ Context Started.]");
			
			TransactionService service = (TransactionService) context.getBean("transactionService");
			
			
			service.transferAmount(executionObj.senderAccountNumber, executionObj.receiverAccountNumber,
					executionObj.amountToSend);
			
			
			
			Runtime.getRuntime().addShutdownHook(new Thread() 
			{
				@Override
				public void run() 
				{

					context.close();
				}
			});
			
			await();
			
		} catch (Throwable e) {
			log.info("[ Exception in Program ", e);

		}
		
		

	}
	
	public static void shutDown()
    {
        latch.countDown();
    }

    /**
     * Await the reception of the shutdown signal.
     */
    public static void await()
    {
        try
        {
            latch.await();
        }
        catch (final InterruptedException ignore)
        {
            Thread.currentThread().interrupt();
        }
    }

}
