package ct.common.spring.example4;



import org.apache.log4j.Logger;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public class TransactionServiceImpl implements TransactionService {
	
	
	private PlatformTransactionManager transactionManager;

	
	
	private TransactionDao transactionDao;
	
	private static final Logger log = Logger.getLogger(TransactionServiceImpl.class);


	public void transferAmount(int senderAccountNumber, int receiverAccountNumber, int amountToSend) {

		log.info("[transaction proceesing..]");
		TransactionStatus txStatus   = null;
		
		try {
		TransactionDefinition txDef = new DefaultTransactionDefinition();

		txStatus = transactionManager.getTransaction(txDef);

		transactionDao.doTransfer(senderAccountNumber, receiverAccountNumber, amountToSend);


			transactionManager.commit(txStatus);

		} catch (Exception e) {
			log.info("[Exception processing Tx]",e);
			transactionManager.rollback(txStatus);

		}

	}

	public PlatformTransactionManager getTransactionManager() {
		return transactionManager;
	}

	public void setTransactionManager(PlatformTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	

	public TransactionDao getTransactionDao() {
		return transactionDao;
	}

	public void setTransactionDao(TransactionDao daoTransaction) {
		this.transactionDao = daoTransaction;
	}

}
