package ct.common.spring.example5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class LeiLoader {

    public static void main(String[] args) {
//        String filePath = "largefile.csv"; // Replace with the path to your large CSV file
        String filePath = "C:\\mydata\\git\\opensource\\docs-LEI-KNTX\\3.6\\20240904-0800-gleif-goldencopy-lei2-golden-copy.csv";

        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                processLine(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void processLine(String line) {
        // Split the line by comma
        String[] columns = line.split(",", 3); // Limit to 3 to handle potential extra commas

        // Ensure there are at least two columns
        if (columns.length >= 2) {
            String column1 = columns[0].trim(); // Extract the first column
            String column2 = columns[1].trim(); // Extract the second column
            // Process the columns as needed
            System.out.println("Column 1: " + column1 + ",\t\t Column 2: " + column2);
        }
    }
}
