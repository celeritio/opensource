package ct.common.spring.example4;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.jdbc.core.JdbcTemplate;

public class TransactionDao {

	private static final String UPDATE_ACCOUNT_BALANCE_QUERY = "UPDATE ACCOUNT SET BALANCE = ? WHERE ACCOUNT_NUMBER = ?";
	private static final String GET_ACCOUNT_BALANCE_QUERY = "SELECT BALANCE FROM ACCOUNT WHERE ACCOUNT_NUMBER = ?";
	private static final String ACCOUNT_VERIFICATION_QUERY = "SELECT 1 FROM ACCOUNT WHERE ACCOUNT_NUMBER = ?";

	private static final Logger log = Logger.getLogger(TransactionDao.class);

	private JdbcTemplate jdbcTemplate;

	/**
	 * Make a transaction
	 */
	public void doTransfer(int senderAccountNumber, int receiverAccountNumber, int amountToSend) {

		int account1Balance;
		int account2Balance;
		int account1NewBalance;
		int account2NewBalance;

		verifyAccount(senderAccountNumber);

		verifyAccount(receiverAccountNumber);

		log.info("[Transfer amount: " + amountToSend + "]");

		if (amountToSend <= 0) {
			log.info("[Input Value is less than 0]");
			throw new RuntimeException(" validation failed");
		}

		account1Balance = getAccountBalance(senderAccountNumber);

		if (account1Balance < amountToSend || account1Balance <= 0) {
			log.info("[Cannot transfer, account doesn't have enough funds!]");
			throw new RuntimeException(" validation failed");
		}

		account1NewBalance = account1Balance - amountToSend;

		updateAccountBalance(senderAccountNumber, account1NewBalance);

		account2Balance = getAccountBalance(receiverAccountNumber);

		account2NewBalance = account2Balance + amountToSend;

		updateAccountBalance(receiverAccountNumber, account2NewBalance);

	}

	private void updateAccountBalance(int accountNumber, int amountToUpdate) {
		jdbcTemplate.update(UPDATE_ACCOUNT_BALANCE_QUERY, amountToUpdate, accountNumber);
	}

	private int getAccountBalance(int accountNumber) {
		return jdbcTemplate.queryForObject(GET_ACCOUNT_BALANCE_QUERY, new Object[] { accountNumber }, Integer.class);
	}

	private boolean verifyAccount(int accountNumber) {
		boolean account;
		try {
			account = jdbcTemplate.queryForObject(ACCOUNT_VERIFICATION_QUERY, new Object[] { accountNumber },
					Integer.class) == 1;
		} catch (Exception e) {
			throw new RuntimeException("Account verification fail !");
		}
		return account;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	@Required
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
