package ct.common.spring.example5;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class CSVColumnReader {

    public static void leiLoader() {
//        String csvFilePath = "largefile.csv"; // Path to your large CSV file
    	
        File file = new File("C:\\mydata\\git\\opensource\\docs-LEI-KNTX\\Few-goldencopy.csv");
        List<String> columnsToRead = Arrays.asList("LEI", "Entity.LegalName", "Entity.LegalName.xmllang", "Pratik"); // List of columns you want to read

        // Convert column names to a set for quick lookup
        Set<String> columnsToReadSet = columnsToRead.stream()
                .map(String::trim)
                .collect(Collectors.toSet());

        try (Reader reader = new FileReader(file);
             CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader())) {

            // Stream through each record and process only the required columns
            Stream<CSVRecord> recordStream = csvParser.getRecords().stream();
            recordStream.forEach(record -> {
                columnsToReadSet.forEach(column -> {
                	 if (record.toMap().containsKey(column)) {
                         String value = record.get(column);
                         System.out.print(value + "\t"); // Print values separated by tab
                     } else {
                        // We comment Else to not do anything
                    	// System.out.print("N/A" + "\t"); // Print placeholder for missing columns
                     }
                });
                System.out.println(); // Newline after each record
            });
            


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}







