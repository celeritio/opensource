## Crud
This is an Spring Boot API Example

## Setup Instruction and Requirements
1. Maven ( Must have to Run or Compile)
1. JDK __1.8.i__
1. Eclipse is the preferred IDE , The code was tested on Eclipse and the corresponding files were committed as well.
1. Postman API

## Maven Setup

For maven Setting of JAVA_HOME is an important step. make sure you have that setup
The instruction for installing / setting up maven can be found  here [MAVEN INSTALL](https://maven.apache.org/install.html)

### Testing the Setup

If maven setup is done. One will have to go the location where the folder was unzipped. say __"c:\pune-java-enthusiasts"__  the folder has sub folders spring/spring-boot navigate inside the folder and execute below command


__This is very Important Step__  we want to save setup time and maven may take time downloading dependencies. its better to do this before reaching for the meetup.


```
mvn clean install
```
The dependencies should automatically be downloaded and Build Success message should be displayed.

### Importing the project in Eclipse or IntelliJ
* Eclipse 

In eclipse file > import... > under Maven select "existing maven project"

once the project is imported right click the project > Maven > Update Project...

This should setup all classpath and Sources for the project in Eclipse

* IntelliJ
 File > new >Project from Existing Sources >> navigate to __pune-java-enthusiasts\spring\spring-boot__  select ok and follow the instruction if prompted for maven project type select that.


## Running Application in Eclipse
* Eclipse 

In Eclipse, Rigth click on Main Class > Run As > Java Application.

once project started open a Browser there we can access the H2 Database console at the following URL: http://localhost:port number/h2-console/

once project started, open terminal execute command > postman

Note:- Postman API knowledge is necessary to work on Application.



