package com.spring.crud.service;

import java.util.List;

import com.spring.crud.model.Employee;

public interface EmployeeService {

	
	/** @param employee : the employee details contained */
	int saveEmployeeDetails(Employee employee);
	
	
	
	/** @param employee : the employee details contained
	 * @param id : the employee identifier
	 * */
	int updateEmployeeDetails(Employee employee, int id);
	
	
	
	/**@param id : the employee identifier*/
	int deleteEmployeeDetails(int id);
	
		
	/**@param id : the employee identifier*/
	Employee getEmployeeById(int id);
	
	
	
	List<Employee> getAllEmployee();

}
