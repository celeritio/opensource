package com.spring.crud.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.spring.crud.model.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private String insertEmpQuery = "INSERT INTO EMPLOYEE(NAME,EMAIL,DEPARTMENT) VALUES(?,?,?)";
	
	private String updateEmpQuery = "UPDATE EMPLOYEE SET NAME=?,EMAIL=?,DEPARTMENT=? WHERE ID=?";
	
	private String deleteEmpQuery = "DELETE FROM EMPLOYEE WHERE ID=?";
	
	private String empListQuery = "SELECT * FROM EMPLOYEE";
	
	private String empByIdQuery = "SELECT * FROM EMPLOYEE WHERE ID=?";

	private static final Logger log = Logger.getLogger(EmployeeServiceImpl.class);

	public int saveEmployeeDetails(Employee employee) 
	{
		log.info("[Saving Employee Details" + "]");
	
		return jdbcTemplate.update(insertEmpQuery,
				new Object[] { employee.getName(), employee.getEmail(), employee.getDepartment() });
	}

	public int updateEmployeeDetails(Employee employee, int id) 
	{
		log.info("[Updating Employee Details" + "]");
	
		return jdbcTemplate.update(updateEmpQuery,
				new Object[] { employee.getName(), employee.getEmail(), employee.getDepartment(), id });
	}

	public int deleteEmployeeDetails(int id) 
	{
		log.info("[Deleting Employee Details" + "]");
		return jdbcTemplate.update(deleteEmpQuery, id);
	}

	public List<Employee> getAllEmployee() 
	{
		log.info("[geting Employee Details By Id" + "]");
	
		return jdbcTemplate.query(empListQuery, new BeanPropertyRowMapper<Employee>(Employee.class));
	}

	public Employee getEmployeeById(int id) {

		log.info("[Geting Employee Details" + "]");

		return jdbcTemplate.queryForObject(empByIdQuery, new BeanPropertyRowMapper<Employee>(Employee.class), id);
	}

}
