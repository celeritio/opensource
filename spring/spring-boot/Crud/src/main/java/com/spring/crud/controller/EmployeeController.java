package com.spring.crud.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.crud.model.Employee;
import com.spring.crud.service.EmployeeService;
import com.spring.crud.service.EmployeeServiceImpl;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService employeeDao;

	private static final Logger log = Logger.getLogger(EmployeeController.class);

	@GetMapping("/getEmployeeDetails")
	public List<Employee> getEmployee() {
		log.info("[ List of Emoployees ]");
		return employeeDao.getAllEmployee();
		
	}

	@GetMapping("/getEmployeeById/{id}")
	public Employee getEmployeeById(@PathVariable int id) {
		log.info("[ Searching Employee By Id = " + id + "]");
		Employee getEmployeeById= employeeDao.getEmployeeById(id);
		log.info("[ Records of  ID " + id + "]");
		return getEmployeeById;
		
	}

	@PostMapping("/saveEmployeeDetails")
	public String saveEmployee(@RequestBody Employee employee) {
		log.info("[ Saving Employee = " + employee + "]");
		int saveEmployeeDetails = employeeDao.saveEmployeeDetails(employee);
		log.info("[ Record Affected  " + saveEmployeeDetails + "]");
		return String.valueOf(saveEmployeeDetails);
	}

	@PutMapping("/updateEmployeeDetails/{id}")
	public String updateEmployee(@RequestBody Employee employee, @PathVariable int id) {
		log.info("[Updating Employee "+employee+"]");
		int updateEmployeeDetails = employeeDao.updateEmployeeDetails(employee, id);
		log.info("[Record Affected"+updateEmployeeDetails +"]");
		return String.valueOf(updateEmployeeDetails);
		
	}

	@DeleteMapping("/deleteEmployeeDetails/{id}")
	public String deleteEmployeeById(@PathVariable int id) {
		log.info("[Deleting Employee By Id:"+id+"]");
		int deleteEmployeeDetails = employeeDao.deleteEmployeeDetails(id);
		log.info("[Record Affected"+deleteEmployeeDetails +"]");
		return String.valueOf(deleteEmployeeDetails);
	}

}
