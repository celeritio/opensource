package com.spring.crud.service;



import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.log4j.Logger;
import org.springframework.context.Lifecycle;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.stereotype.Component;


@Component
public class Application{

	private static final Logger log = Logger.getLogger(Application.class);

	
	

	/**
	 * Can be used to initialize your Application startups
	 * 
	 */
	@PostConstruct
	public void start()
	{
		log.info("[Start of bean implementing the LifeCycle Interface Called ]");
		
		
		EmbeddedDatabase 	db = new EmbeddedDatabaseBuilder()
    		.setType(EmbeddedDatabaseType.H2)
    		.addScript("db.sql")
    		.build();
	}

	/**
	 * Can be used to destroy your Application lifecycle
	 */
	@PreDestroy
	public void stop()
	{
		log.info("[Stop of bean implementing the LifeCycle Interface Called ]");

	}

	
	
}
